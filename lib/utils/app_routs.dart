import 'package:flutter/material.dart';

import '../screens/add_task_screen.dart';
import '../screens/all_tasks_screen.dart';
import '../screens/home_screen.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case HomeScreen.id:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
      case AllTasksScreen.id:
        return MaterialPageRoute(builder: (_) => const AllTasksScreen());
      case AddTask.id:
        final args = routeSettings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
            builder: (_) => AddTask(
                  task: args['task'],
                  isRead: args['isRead'],
                  index: args['index'],
                  rout: args['rout'] ?? RoutD.fromHomeScreen,
                ));

      default:
        return null;
    }
  }
}
