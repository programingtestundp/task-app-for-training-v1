part of 'all_tasks_cubit.dart';

enum DismissStatus {
  non,
  editing,
  deleting,
  confirmEdit,
  confirmDelete,
  errorWithDeleting
}

enum AppStatus { loading, loaded, error }

class AllTasksState {
  //al tasks list
  final List<TaskModel> tasksList;
  // the dismiss status for the elements in the tasks list
  final List<DismissStatus> dismissStatusList;
  // for the containers of the elements in the tasks list
  final List<Radius> radius;
  // the situation of the app depend on fetching data and initial the state
  final AppStatus appStatus;
  // if there is any error happened when the data fetching
  final String errorMessage;
  AllTasksState({
    required this.dismissStatusList,
    List<Radius>? radius,
    required this.tasksList,
    this.errorMessage = '',
    this.appStatus = AppStatus.loading,
  }) : radius = radius ?? [Radius.circular(10.r), Radius.circular(10.r)];

  AllTasksState copyWith(
      {List<DismissStatus>? dismissStatusList,
      List<Radius>? radius,
      List<TaskModel>? tasksList,
      String? errorMessage,
      AppStatus? appStatus}) {
    return AllTasksState(
      appStatus: appStatus ?? this.appStatus,
      tasksList: tasksList ?? this.tasksList,
      radius: radius ?? this.radius,
      errorMessage: errorMessage ?? this.errorMessage,
      dismissStatusList: dismissStatusList ?? this.dismissStatusList,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'dismissStatusList': dismissStatusList
          .map((status) => status.toString().split('.').last)
          .toList(), // Convert list of enums to list of strings,
      "tasks":
          List.generate(tasksList.length, (index) => tasksList[index].toJson())
    };
  }

  factory AllTasksState.fromMap(Map<String, dynamic> map) {
    return AllTasksState(
        dismissStatusList:
            (map['dismissStatusList'] as List<dynamic>).map((status) {
          return DismissStatus.values.firstWhere(
            (enumValue) => enumValue.toString().split('.').last == status,
          );
        }).toList(),
        tasksList: List.generate(map['tasks'].length,
            (index) => TaskModel.fromJson(map['tasks'][index])));
  }

  String toJson() => json.encode(toMap());

  factory AllTasksState.fromJson(String source) =>
      AllTasksState.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AllTasksState(dismissStatusList: $dismissStatusList,tasksList: $tasksList,errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(covariant AllTasksState other) {
    if (identical(this, other)) return true;

    return listEquals(other.dismissStatusList, dismissStatusList) &&
        listEquals(other.radius, radius) &&
        listEquals(other.tasksList, tasksList) &&
        other.errorMessage == errorMessage;
  }

  @override
  int get hashCode {
    return dismissStatusList.hashCode ^
        radius.hashCode ^
        tasksList.hashCode ^
        errorMessage.hashCode;
  }
}
