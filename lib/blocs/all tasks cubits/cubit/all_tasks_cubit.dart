import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:built_collection/built_collection.dart';

import '../../../data/server/exceptions_hundler.dart';
import '../../../data/server/tasks_service.dart';
import '../../../models/task_model.dart';

part 'all_tasks_state.dart';

class AllTasksCubit extends Cubit<AllTasksState> {
  AllTasksCubit()
      : super(AllTasksState(
          dismissStatusList: [],
          tasksList: [],
        ));
  final api = TasksService.create();

  FutureOr initialState() async {
    debugPrint('initialState start');

    final result = await HandleException.handle(api.getTasks);
    if (result.success != null) {
      debugPrint('initialState success-1');
      final response =
          result.success!.response as Response<BuiltList<TaskModel>>;
      debugPrint('initialState success-1');

      final tasks = response.body;
      debugPrint('initialState success-1');

      List<DismissStatus>? dismissStatusList =
          List.generate(tasks?.length ?? 0, (index) => DismissStatus.non);

      debugPrint('initialState: $tasks');
      final newState = state.copyWith(
          dismissStatusList: dismissStatusList,
          appStatus: AppStatus.loaded,
          errorMessage: '',
          tasksList: tasks?.toList());
      emit(newState);
      debugPrint('initialState is success');
    } else if (result.failure != null) {
      final errorMessage = result.failure!.message;
      debugPrint('initialState - Error: $errorMessage');
      final newState = state.copyWith(
        appStatus: AppStatus.error,
        errorMessage: errorMessage,
      );
      emit(newState);
    }
  }

  FutureOr retry() async {
    final newState = state.copyWith(
        dismissStatusList: [],
        appStatus: AppStatus.loading,
        errorMessage: '',
        tasksList: []);
    emit(newState);

    Future.delayed(
      const Duration(
        seconds: 1,
      ),
      () {
        initialState();
      },
    );
  }

  void updateTasks({required TaskModel task, int? index}) {
    if (index == null) {
// add task
      final newState =
          state.copyWith(tasksList: List.from(state.tasksList)..add(task));

      emit(newState);
    } else {
      //edit task
      final List<TaskModel> tasks = List.from(state.tasksList)
        ..removeAt(index)
        ..insert(index, task);
      final newState = state.copyWith(tasksList: tasks);
      emit(newState);
    }
  }

  void setDismissStatus(
      {required DismissStatus dismissStatus, required int index}) {
    final List<DismissStatus> newDismissStatusList =
        List.from(state.dismissStatusList)..[index] = dismissStatus;
    final newState = state.copyWith(dismissStatusList: newDismissStatusList);
    emit(newState);
  }

// check if the update stated and its direction
  void onUpdateDismissible(
      {required DismissUpdateDetails details, required int index}) {
    final radiusDefault = Radius.circular(10.r);
    const radiusZero = Radius.zero;
    final List<DismissStatus> newDismissStatusList =
        List.from(state.dismissStatusList);
    List<Radius> newRadiuses = List.from(state.radius);
    if (details.progress > 0) {
      //is deleting
      if (details.direction == DismissDirection.endToStart &&
          !(state.dismissStatusList[index] == DismissStatus.deleting)) {
        newDismissStatusList[index] = DismissStatus.deleting;
        newRadiuses = [radiusDefault, radiusZero];
      }
      //is edit
      else if (details.direction == DismissDirection.startToEnd &&
          !(state.dismissStatusList[index] == DismissStatus.editing)) {
        newDismissStatusList[index] = DismissStatus.editing;
        newRadiuses = [radiusZero, radiusDefault];
      }
    }
    //reset
    else if (state.dismissStatusList[index] == DismissStatus.editing ||
        state.dismissStatusList[index] == DismissStatus.deleting) {
      newDismissStatusList[index] = DismissStatus.non;
      newRadiuses = [radiusZero, radiusZero];
    }
    final newState = state.copyWith(
        dismissStatusList: newDismissStatusList, radius: newRadiuses);
    emit(newState);
  }

  Future<bool> confirmDelete({required int index}) async {
    //confirm delete

    final List<DismissStatus> newDismissStatusList =
        List.from(state.dismissStatusList);
    newDismissStatusList[index] = DismissStatus.deleting;
    emit(state.copyWith(dismissStatusList: newDismissStatusList));
    await Future.delayed(Duration(seconds: 5));

    final result = await HandleException.handle(() async => api
        .deleteTask(state.tasksList[state.tasksList.length - 1 - index].id!));
    if (result.success != null) {
      final List<TaskModel> tasks = result.success!.response.body.toList();

      debugPrint('confirmDelete - success: ${result.success!.response.body}');
      newDismissStatusList[index] = DismissStatus.confirmDelete;
      final newState = state.copyWith(
          errorMessage: '',
          tasksList: tasks,
          dismissStatusList: newDismissStatusList);

      emit(newState);
      return true;
    } else if (result.failure != null) {
      final errorMessage = result.failure!.message;
      debugPrint('confirmDelete - Error: $errorMessage');
      newDismissStatusList[index] = DismissStatus.errorWithDeleting;
      final newState = state.copyWith(
          errorMessage: errorMessage, dismissStatusList: newDismissStatusList);
      emit(newState);
      return false;
    } else {
      return false;
    }
  }
}
