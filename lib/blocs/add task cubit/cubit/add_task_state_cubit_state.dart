part of 'add_task_state_cubit_cubit.dart';

@immutable
class AddTaskStateCubitState {
  // wether if the keyboard open or not
  final bool isKeyboardOpenKey;
  //list of all error messages
  final List<String> errorMessages;
  //the task to add or edit
  final TaskModel task;
//network or server error
  final String netError;
  //loading data state
  final bool isLoading;
  AddTaskStateCubitState({
    this.isKeyboardOpenKey = false,
    TaskModel? task,
    this.netError = '',
    this.isLoading = false,
    this.errorMessages = const [],
  }) : task = task ??
            TaskModel(
              (b) => b
                ..task_name = ''
                ..task_details = '',
            );

  AddTaskStateCubitState copyWith(
      {bool? isKeyboardOpenKey,
      List<String>? errorMessages,
      String? netError,
      String? detailsError,
      bool? isLoading,
      TaskModel? task}) {
    return AddTaskStateCubitState(
      task: task ?? this.task,
      isLoading: isLoading ?? this.isLoading,
      netError: netError ?? this.netError,
      isKeyboardOpenKey: isKeyboardOpenKey ?? this.isKeyboardOpenKey,
      errorMessages: errorMessages ?? this.errorMessages,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'isKeyboardOpenKey': isKeyboardOpenKey,
      'errorMessages': errorMessages,
    };
  }

  factory AddTaskStateCubitState.fromMap(Map<String, dynamic> map) {
    return AddTaskStateCubitState(
      isKeyboardOpenKey: map['isKeyboardOpenKey'] as bool,
      errorMessages: List<String>.from(map['errorMessages'] ?? []),
    );
  }

  String toJson() => json.encode(toMap());

  factory AddTaskStateCubitState.fromJson(String source) =>
      AddTaskStateCubitState.fromMap(
        json.decode(source) as Map<String, dynamic>,
      );

  @override
  String toString() =>
      'AddTaskStateCubitState(isKeyboardOpenKey: $isKeyboardOpenKey, errorMessages: $errorMessages, )';

  @override
  bool operator ==(covariant AddTaskStateCubitState other) {
    if (identical(this, other)) return true;

    return other.isKeyboardOpenKey == isKeyboardOpenKey &&
        listEquals(other.errorMessages, errorMessages) &&
        other.task == task &&
        other.netError == netError &&
        other.isLoading == isLoading;
  }

  @override
  int get hashCode =>
      isKeyboardOpenKey.hashCode ^
      errorMessages.hashCode ^
      task.hashCode ^
      netError.hashCode ^
      isLoading.hashCode;
}
