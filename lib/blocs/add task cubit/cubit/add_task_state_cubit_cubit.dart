import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';

import '../../../data/server/exceptions_hundler.dart';
import '../../../data/server/tasks_service.dart';
import '../../../models/task_model.dart';

part 'add_task_state_cubit_state.dart';

class AddTaskStateCubitCubit extends Cubit<AddTaskStateCubitState> {
  AddTaskStateCubitCubit() : super(AddTaskStateCubitState());
  final api = TasksService.create();

  void initialState({bool isKeyboardOpenKey = false}) {
    emit(state.copyWith(isKeyboardOpenKey: isKeyboardOpenKey));
  }

  void setKeyboardKey({required bool key}) {
    final newState = state.copyWith(isKeyboardOpenKey: key);
    emit(newState);
  }

  void setErrorMessage({String? errorMessage, String? fieldName}) {
    if (errorMessage != null && !state.errorMessages.contains(fieldName)) {
      //add error message
      List<String> errorMessages = List.from(state.errorMessages)
        ..add('$fieldName: $errorMessage');
      final newState = state.copyWith(
        errorMessages: errorMessages,
      );

      emit(newState);
    } else if (fieldName != null && state.errorMessages.contains(fieldName)) {
      //remove error message
      List<String> errorMessages = List.from(state.errorMessages)
        ..removeWhere((element) => element.contains(fieldName));
      final newState = state.copyWith(errorMessages: errorMessages);

      emit(newState);
    }
  }

  Future<String> addEditTask({required TaskModel task, String? id}) async {
    final newState = state.copyWith(isLoading: true);
    emit(newState);
    await Future.delayed(
      const Duration(
        milliseconds: 500,
      ),
    );
    final result = await HandleException.handle(() async =>
        id == null ? api.createTask(task) : api.updateTask(id, task));
    if (result.success != null) {
      final task = result.success!.response.body;
      debugPrint('addTask - success: ${result.success!.response.body}');
      final newState =
          state.copyWith(netError: '', task: task, isLoading: false);
      emit(newState);
      return '';
    } else if (result.failure != null) {
      final errorMessage = result.failure!.message;
      debugPrint('addTask - Error: $errorMessage');
      final newState = state.copyWith(netError: errorMessage, isLoading: false);
      emit(newState);
      return errorMessage;
    } else {
      return 'Unknown Error';
    }
  }
}
