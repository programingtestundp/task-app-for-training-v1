// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_model.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TaskModel> _$taskModelSerializer = new _$TaskModelSerializer();

class _$TaskModelSerializer implements StructuredSerializer<TaskModel> {
  @override
  final Iterable<Type> types = const [TaskModel, _$TaskModel];
  @override
  final String wireName = 'TaskModel';

  @override
  Iterable<Object?> serialize(Serializers serializers, TaskModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'task_name',
      serializers.serialize(object.task_name,
          specifiedType: const FullType(String)),
      'task_details',
      serializers.serialize(object.task_details,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.date;
    if (value != null) {
      result
        ..add('date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  TaskModel deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'task_name':
          result.task_name = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'task_details':
          result.task_details = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskModel extends TaskModel {
  @override
  final String? id;
  @override
  final String task_name;
  @override
  final String task_details;
  @override
  final String? date;

  factory _$TaskModel([void Function(TaskModelBuilder)? updates]) =>
      (new TaskModelBuilder()..update(updates))._build();

  _$TaskModel._(
      {this.id, required this.task_name, required this.task_details, this.date})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(task_name, r'TaskModel', 'task_name');
    BuiltValueNullFieldError.checkNotNull(
        task_details, r'TaskModel', 'task_details');
  }

  @override
  TaskModel rebuild(void Function(TaskModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskModelBuilder toBuilder() => new TaskModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskModel &&
        id == other.id &&
        task_name == other.task_name &&
        task_details == other.task_details &&
        date == other.date;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, task_name.hashCode);
    _$hash = $jc(_$hash, task_details.hashCode);
    _$hash = $jc(_$hash, date.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'TaskModel')
          ..add('id', id)
          ..add('task_name', task_name)
          ..add('task_details', task_details)
          ..add('date', date))
        .toString();
  }
}

class TaskModelBuilder implements Builder<TaskModel, TaskModelBuilder> {
  _$TaskModel? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _task_name;
  String? get task_name => _$this._task_name;
  set task_name(String? task_name) => _$this._task_name = task_name;

  String? _task_details;
  String? get task_details => _$this._task_details;
  set task_details(String? task_details) => _$this._task_details = task_details;

  String? _date;
  String? get date => _$this._date;
  set date(String? date) => _$this._date = date;

  TaskModelBuilder();

  TaskModelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _task_name = $v.task_name;
      _task_details = $v.task_details;
      _date = $v.date;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskModel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TaskModel;
  }

  @override
  void update(void Function(TaskModelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  TaskModel build() => _build();

  _$TaskModel _build() {
    final _$result = _$v ??
        new _$TaskModel._(
            id: id,
            task_name: BuiltValueNullFieldError.checkNotNull(
                task_name, r'TaskModel', 'task_name'),
            task_details: BuiltValueNullFieldError.checkNotNull(
                task_details, r'TaskModel', 'task_details'),
            date: date);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
