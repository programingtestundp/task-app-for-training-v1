import 'dart:convert';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import '../data/serialization/serializers.dart';
part 'task_model.g.dart';

abstract class TaskModel implements Built<TaskModel, TaskModelBuilder> {
  /// The unique ID of the task.
  String? get id;

  /// The name of the task.
  String get task_name;

  /// The details of the task.
  String get task_details;

  /// The date of the task.
  String? get date;

  /// Private constructor to prevent direct instantiation.
  TaskModel._();

  /// Factory constructor for creating a new instance of the [TaskModel] class.
  ///
  /// This constructor can be used to create a new [TaskModel] instance with the provided updates.
  factory TaskModel([Function(TaskModelBuilder b) updates]) = _$TaskModel;

  /// Converts the [TaskModel] instance to a JSON string.
  ///
  /// Returns a JSON string representation of the [TaskModel] instance.
  String toJson() {
    return json.encode(serializers.serializeWith(TaskModel.serializer, this));
  }

  /// Creates a new [TaskModel] instance from a JSON string.
  ///
  /// Returns a new [TaskModel] instance created from the provided JSON string.
  static TaskModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        TaskModel.serializer, json.decode(jsonString))!;
  }

  /// The serializer for the [TaskModel] class.
  ///
  /// This serializer is used to serialize and deserialize [TaskModel] instances.
  static Serializer<TaskModel> get serializer => _$taskModelSerializer;

  /// Creates a copy of the [TaskModel] instance with the specified fields updated.
  ///
  /// Returns a new [TaskModel] instance with the specified fields updated.
  TaskModel copyWith({
    String? id,
    String? task_name,
    String? task_details,
    String? date,
  }) {
    return rebuild((b) => b
      ..id = id ?? this.id
      ..task_name = task_name ?? this.task_name
      ..task_details = task_details ?? this.task_details
      ..date = date ?? this.date);
  }
}
