import 'package:chopper/chopper.dart';
// import 'package:chopper_1/models/built_post.dart';
import 'package:built_collection/built_collection.dart';

import 'serializers.dart';

class BuiltValueConverter extends JsonConverter {
  @override
  Request convertRequest(Request request) {
    return super.convertRequest(request.copyWith(
        body: serializers.serializeWith(
            serializers.serializerForType(request.body.runtimeType)!,
            request.body)));
  }

  @override
  Future<Response<BodyType>> convertResponse<BodyType, SingleItemType>(
    Response response,
  ) async {
    final Response dynamicResponse = await super.convertResponse(response);
    final BodyType customBody =
        _convertToCustomObject<SingleItemType>(dynamicResponse.body);
    return dynamicResponse.copyWith(body: customBody);
  }

  dynamic _convertToCustomObject<SingleItemType>(dynamic element) {
    if (element is SingleItemType) return element;
    if (element is List) {
      return _deserializeListOf<SingleItemType>(element);
    } else {
      return _deserialize<SingleItemType>(element);
    }
  }

  BuiltList<SingleItemType> _deserializeListOf<SingleItemType>(
      List dynamicList) {
    return BuiltList<SingleItemType>(
        dynamicList.map((e) => _deserialize<SingleItemType>(e)));
  }

  SingleItemType _deserialize<SingleItemType>(Map<String, dynamic> value) {
    return serializers.deserializeWith(
        serializers.serializerForType(SingleItemType)!, value);
  }
}
