import 'package:chopper/chopper.dart';

import '../../models/task_model.dart';
import 'package:built_collection/built_collection.dart';

import '../serialization/built_value_converter.dart';

part 'tasks_service.chopper.dart';

//TasksService class
@ChopperApi()
abstract class TasksService extends ChopperService {
  //getTasks method
  @Get(
    path: '/gettasks',
  )
  Future<Response<BuiltList<TaskModel>>> getTasks();

  //getTask method
  @Get(path: '/gettask/{id}')
  Future<Response<TaskModel>> getTask(@Path('id') String id);

  //createTask method
  @Post(path: '/createtask')
  Future<Response<TaskModel>> createTask(@Body() TaskModel task);

  //deleteTask method
  @Delete(path: 'deletetask/{id}')
  Future<Response<BuiltList<TaskModel>>> deleteTask(@Path('id') String id);

  //updateTask method
  @Put(path: 'updatetask/{id}')
  Future<Response<TaskModel>> updateTask(
      @Path('id') String id, @Body() TaskModel task);

  // This is a static factory method to create a new instance of TasksService
  static TasksService create() {
    final client = ChopperClient(
        baseUrl: Uri.tryParse('localhost'),
        services: [_$TasksService()],
        converter: BuiltValueConverter(),
        interceptors: [
          (Request request) async {
            if (request.method == HttpMethod.Post) {
              chopperLogger.info('Performed a POST request');
            }
            return request;
          },
        ]);
    return _$TasksService(client);
  }
}
