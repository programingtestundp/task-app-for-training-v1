// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tasks_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations, unnecessary_brace_in_string_interps
final class _$TasksService extends TasksService {
  _$TasksService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = TasksService;

  @override
  Future<Response<BuiltList<TaskModel>>> getTasks() {
    final Uri $url = Uri.parse('/gettasks');
    final Request $request = Request(
      'GET',
      $url,
      client.baseUrl,
    );
    return client.send<BuiltList<TaskModel>, TaskModel>($request);
  }

  @override
  Future<Response<TaskModel>> getTask(String id) {
    final Uri $url = Uri.parse('/gettask/${id}');
    final Request $request = Request(
      'GET',
      $url,
      client.baseUrl,
    );
    return client.send<TaskModel, TaskModel>($request);
  }

  @override
  Future<Response<TaskModel>> createTask(TaskModel task) {
    final Uri $url = Uri.parse('/createtask');
    final $body = task;
    final Request $request = Request(
      'POST',
      $url,
      client.baseUrl,
      body: $body,
    );
    return client.send<TaskModel, TaskModel>($request);
  }

  @override
  Future<Response<BuiltList<TaskModel>>> deleteTask(String id) {
    final Uri $url = Uri.parse('deletetask/${id}');
    final Request $request = Request(
      'DELETE',
      $url,
      client.baseUrl,
    );
    return client.send<BuiltList<TaskModel>, TaskModel>($request);
  }

  @override
  Future<Response<TaskModel>> updateTask(
    String id,
    TaskModel task,
  ) {
    final Uri $url = Uri.parse('updatetask/${id}');
    final $body = task;
    final Request $request = Request(
      'PUT',
      $url,
      client.baseUrl,
      body: $body,
    );
    return client.send<TaskModel, TaskModel>($request);
  }
}
