import 'dart:io';

import 'package:flutter/rendering.dart';
import 'package:http/http.dart';

class HandleException {
  static Future<
      ({
        Failure<dynamic, Exception>? failure,
        Success<dynamic, Exception>? success
      })> handle(Function request) async {
    Failure<dynamic, Exception>? failure;
    Success<dynamic, Exception>? success;
    try {
      final response = await (request).call().timeout(
        Duration(seconds: 10),
        onTimeout: () {
          throw TimeOutException();
        },
      );

      if (response.statusCode == 200) {
        success = Success(response);
      } else {
        failure = Failure(statusCode: response.statusCode);
      }
    } on Exception catch (e) {
      failure = Failure(exception: e);
    }
    return (success: success, failure: failure);
  }
}

sealed class Result<S, E extends Exception> {
  const Result();
}

final class Success<S, E extends Exception> extends Result<S, E> {
  Success(this.response) {
    message = 'Success';
  }
  final S response;
  late String message;
}

final class Failure<S, E extends Exception> extends Result<S, E> {
  Failure({this.exception, this.statusCode}) {
    if (exception != null) {
      if (exception is SocketException) {
        message = (exception as SocketException).message;
        exceptionType = 'SocketException';
      } else if (exception is HttpException) {
        message = (exception as HttpException).message;
        exceptionType = 'HttpException';
      } else if (exception is FormatException) {
        message = (exception as FormatException).message;

        exceptionType = 'FormatException';
      } else if (exception is ClientException) {
        message = (exception as ClientException).message;

        exceptionType = 'ClientException';
      } else if (exception is TimeOutException) {
        message = (exception as TimeOutException).message;

        exceptionType = 'TimeOutException';
      } else {
        message = 'Unknown error occurred';
        exceptionType = 'Unknown';
      }
    } else if (statusCode != null) {
      message = getErrorMessage(statusCode!);
      exceptionType = 'Request Error';
    }

    debugPrint('Exception Type: $exceptionType, Message: $message');
  }

  final E? exception;
  late String message;
  late int? statusCode;
  late String exceptionType;
}

String getErrorMessage(int statusCode) {
  String message = '';
  switch (statusCode) {
    case 400:
      message = 'Bad Request';
      break;
    case 401:
      message = 'Unauthorized';
      break;
    case 402:
      message = 'Payment Required';
      break;

    case 403:
      message = 'Forbidden';
      break;
    case 404:
      message = 'Not Found';
      break;
    case 405:
      message = 'Method Not Allowed';
      break;
    case 406:
      message = 'Not Acceptable';
      break;
    case 407:
      message = 'Proxy Authentication Required';
    case 408:
      message = 'Request Timeout';
    case 409:
      message =
          'The request could not be completed due to a conflict with the current state of the resource.';
    case 410:
      message = 'The requested resource is no longer available at the server.';
    case 411:
      message =
          'The server refuses to accept the request without a defined Content- Length';
    case 412:
      message = 'Precondition Failed';
    case 413:
      message = 'Request Entity Too Large';
    case 414:
      message = 'Request-URI Too Long';
    case 415:
      message = 'Unsupported Media Type';
      break;
    case 416:
      message = 'Requested Range Not Satisfiable';
      break;
    case 417:
      message = 'Expectation Failed';
      break;
    case 418:
      message = 'I\'m a teapot (RFC 2324)';
      break;
    case 420:
      message = 'Enhance Your Calm (Twitter)';
      break;
    case 422:
      message = 'Unprocessable Entity (WebDAV)';
      break;
    case 423:
      message = 'Locked (WebDAV)';
      break;
    case 424:
      message = 'Failed Dependency (WebDAV)';
      break;
    case 425:
      message = 'Too Early (WebDAV)';
      break;
    case 426:
      message = 'Upgrade Required';
      break;
    case 428:
      message = 'Precondition Required';
      break;
    case 429:
      message = 'Too Many Requests';
      break;
    case 431:
      message = 'Request Header Fields Too Large';
      break;
    case 444:
      message = 'No Response (Nginx)';
      break;
    case 449:
      message = 'Retry With (Microsoft)';
      break;
    case 450:
      message = 'Blocked by Windows Parental Controls (Microsoft)';
      break;
    case 451:
      message = 'Unavailable For Legal Reasons';
      break;
    case 499:
      message = 'Client Closed Request (Nginx)';

      break;
    case 500:
      message = 'Internal Server Error';
      break;
    case 501:
      message = 'Not Implemented';
      break;
    case 502:
      message = 'Bad Gateway';
      break;
    case 503:
      message = 'Service Unavailable';
      break;
    case 504:
      message = 'Gateway Timeout';
      break;
    case 505:
      message = 'HTTP Version Not Supported (Experimental)';
      break;
    case 506:
      message = 'Variant Also Negotiates (Experimental)';
      break;
    case 507:
      message = 'Insufficient Storage (WebDAV)';
      break;
    case 508:
      message = 'Loop Detected (WebDAV)';
      break;
    case 510:
      message = 'Not Extended';
      break;
    case 511:
      message = 'Network Authentication Required';
      break;
    default:
      message = 'Unknown error occurred';
  }
  return message;
}

class TimeOutException implements Exception {
  String message;

  TimeOutException({this.message = 'connection time out'});

  @override
  String toString() {
    return "TimeOutException: $message";
  }
}
