import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget(
      {super.key,
      this.height,
      this.width,
      required this.color,
      required this.text,
      required this.onTap,
      this.isLoading = false,
      this.loadingIndictorColor = Colors.white,
      required this.textColor});
  final Color color;
  final String text;
  final Color textColor;
  final void Function() onTap;
  final double? height;
  final double? width;
  final bool isLoading;
  final Color loadingIndictorColor;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width ?? double.maxFinite,
        height: height ?? 60.r,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.r), color: color),
        alignment: Alignment.center,
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(
                color: loadingIndictorColor,
              ))
            : Text(
                text,
                style: TextStyle(color: textColor, fontSize: 20.sp),
              ),
      ),
    );
  }
}
