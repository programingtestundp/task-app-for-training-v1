import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'button_widget.dart';

Future<bool?> confirmDialog(BuildContext context,
    {String statText = "",
    String specialText = "",
    String endText = '',
    Color defaultTextColor = Colors.black,
    Color specialTextColor = Colors.blue,
    Color backgroundColor = Colors.white,
    Color confirmTextColor = Colors.white,
    Color buttonColor = Colors.red,
    double? width,
    double? height,
    void Function()? onTap}) async {
  Completer<bool?>? completer = Completer<bool?>();

  showDialog(
    context: context,
    builder: (context) {
      return Dialog(
        elevation: 6.r,
        child: Container(
            height: height ?? 100.h,
            width: width ?? 250.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.r),
              color: backgroundColor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RichText(
                    text: TextSpan(
                        text: statText,
                        style: TextStyle(
                            color: defaultTextColor,
                            fontWeight: FontWeight.bold),
                        children: [
                      TextSpan(
                          text: specialText,
                          style: TextStyle(color: specialTextColor)),
                      TextSpan(text: endText),
                    ])),
                ButtonWidget(
                    color: buttonColor,
                    width: 120.w,
                    height: 40.h,
                    text: 'confirm',
                    onTap: onTap ?? () {},
                    textColor: confirmTextColor),
              ],
            )),
      );
    },
  ).then((value) {
    if (!completer.isCompleted) {
      completer.complete(value);
    }
  });

  return completer.future;
}
