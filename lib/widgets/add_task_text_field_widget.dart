import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_task_management_app_1/blocs/add%20task%20cubit/cubit/add_task_state_cubit_cubit.dart';
import 'package:flutter_task_management_app_1/utils/app_colors.dart';

class AddTaskTextFieldWidget extends StatelessWidget {
  AddTaskTextFieldWidget(
      {super.key,
      required this.controller,
      required this.hintText,
      required this.fieldName,
      this.isRead = false,
      this.onTapOutside,
      this.maxLines = 1,
      BorderRadius? borderRadius})
      : _borderRadius = borderRadius ?? BorderRadius.circular(30.r);
  final TextEditingController controller;
  final String hintText;
  final void Function(PointerDownEvent)? onTapOutside;
  final int maxLines;
  final BorderRadius _borderRadius;
  final String fieldName;
  final bool isRead;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      readOnly: isRead,
      controller: controller,
      maxLines: maxLines,
      onTap: () {},
      onTapOutside: onTapOutside,
      validator: (value) {
        String? result = validator(value);
        if (result != null) {
          context
              .read<AddTaskStateCubitCubit>()
              .setErrorMessage(errorMessage: result, fieldName: fieldName);
        }
        return result;
      },
      decoration: InputDecoration(
        filled: true,
        fillColor: AppColors.textHolder,
        hintText: hintText,
        enabledBorder: OutlineInputBorder(
          borderRadius: _borderRadius,
          borderSide: BorderSide(
            color: Colors.white,
            width: 1.r,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: _borderRadius,
          borderSide: BorderSide(color: Colors.white, width: 1.r),
        ),
      ),
    );
  }

  String? validator(
    String? value,
  ) {
    if (!(value == null)) {
      if (value.isEmpty) {
        return 'Please enter a text';
      }
      if (!value.contains(RegExp(r'^[a-zA-Z0-9\s]+$'))) {
        return 'Name can only contain alphabetic characters an Numbers';
      }

      // if (value.length > 10) {
      //   return 'Name must be at least 3 characters';
      // }
    }
    return null;
  }
}
