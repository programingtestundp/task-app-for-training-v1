import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TaskWidget extends StatelessWidget {
  const TaskWidget(
      {super.key,
      required this.color,
      required this.text,
      required this.radius});
  final List<Radius> radius;
  final Color color;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 50.h,
      decoration: BoxDecoration(
          color: const Color(0xFFedf0f8),
          borderRadius:
              BorderRadius.horizontal(left: radius[0], right: radius[1])),
      alignment: Alignment.center,
      child: Text(
        text,
        style: TextStyle(fontSize: 20.sp, color: color),
      ),
    );
  }
}
