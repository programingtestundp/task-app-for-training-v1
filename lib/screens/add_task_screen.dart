import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_task_management_app_1/models/task_model.dart';
import 'package:flutter_task_management_app_1/screens/all_tasks_screen.dart';
import 'package:flutter_task_management_app_1/utils/app_colors.dart';
import 'package:flutter_task_management_app_1/widgets/add_task_text_field_widget.dart';
import 'package:flutter_task_management_app_1/widgets/snke_bar_message_widget.dart';

import '../blocs/add task cubit/cubit/add_task_state_cubit_cubit.dart';
import '../blocs/all tasks cubits/cubit/all_tasks_cubit.dart';
import '../widgets/button_widget.dart';

enum RoutD { fromHomeScreen, fromAllTasksScreen }

class AddTask extends StatefulWidget {
  const AddTask(
      {super.key,
      this.task,
      this.rout = RoutD.fromHomeScreen,
      this.index,
      this.isRead = false});
  static const id = 'add_task_screen_id';
  final TaskModel? task;
  final RoutD rout;
  final int? index;
  final bool isRead;
  @override
  State<AddTask> createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  TextEditingController nameController = TextEditingController();

  TextEditingController detailsController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.task != null) {
      nameController.text = widget.task?.task_name ?? '';
      detailsController.text = widget.task?.task_details ?? '';
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    nameController.dispose();
    detailsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
    _checkKeyboard(context);

    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/addtask1.jpg'), fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: false,
        body: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 10.h,
              ),
              //back arrow
              IconButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  icon: const Icon(
                    Icons.arrow_back,
                    color: AppColors.secondaryColor,
                  )),
              SizedBox(
                height: 80.h,
              ),
              // the body where the text field

              BlocSelector<AddTaskStateCubitCubit, AddTaskStateCubitState,
                  bool>(
                selector: (state) {
                  return state.isKeyboardOpenKey;
                },
                builder: (context, isKeyboardOpenKey) {
                  return SizedBox(
                    height: isKeyboardOpenKey ? 222.h : 451.h,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.w),
                      child: SingleChildScrollView(
                        physics: isKeyboardOpenKey
                            ? const AlwaysScrollableScrollPhysics()
                            : const NeverScrollableScrollPhysics(),
                        child: Column(children: [
                          SizedBox(
                            height: isKeyboardOpenKey ? 20.h : 100.h,
                          ),
                          // name textfield
                          AddTaskTextFieldWidget(
                              isRead: widget.isRead,
                              fieldName: 'name',
                              controller: nameController,
                              hintText: 'Task Name',
                              onTapOutside: (event) {
                                debugPrint('onTapOut');
                                SystemChrome.setEnabledSystemUIMode(
                                    SystemUiMode.immersiveSticky);
                                if (!isKeyboardOpenKey) {
                                  FocusScopeNode currentFocus =
                                      FocusScope.of(context);
                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                }
                              }),
                          SizedBox(
                            height: 20.h,
                          ),
                          // details textfield
                          AddTaskTextFieldWidget(
                              isRead: widget.isRead,
                              fieldName: 'details',
                              controller: detailsController,
                              hintText: 'Task Details',
                              maxLines: 3,
                              borderRadius: BorderRadius.circular(15.r)),
                          SizedBox(
                            height: 20.h,
                          ),
                          // add button
                          if (!widget.isRead)
                            ButtonWidget(
                              text: widget.index == null ? 'Add' : 'Edit',
                              color: AppColors.mainColor,
                              textColor: Colors.white,
                              onTap: () {
                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState!.save();
                                  FocusScopeNode currentFocus =
                                      FocusScope.of(context);
                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                  final task = TaskModel(
                                    (b) => b
                                      ..task_name = nameController.text
                                      ..task_details = detailsController.text,
                                  );
                                  context
                                      .read<AddTaskStateCubitCubit>()
                                      .addEditTask(
                                          task: task, id: widget.task?.id)
                                      .then((value) {
                                    if (value == '') {
                                      if (widget.rout == RoutD.fromHomeScreen) {
                                        Navigator.pushReplacementNamed(
                                            context, AllTasksScreen.id);
                                        context
                                            .read<AllTasksCubit>()
                                            .updateTasks(
                                              task: task,
                                            );
                                      } else {
                                        Navigator.of(context)
                                            .maybePop(true)
                                            .then((value) {
                                          context
                                              .read<AllTasksCubit>()
                                              .updateTasks(
                                                  task: task,
                                                  index: widget.index);
                                        });
                                      }
                                    } else {
                                      context.showSnackBar(
                                          message: value,
                                          backgroundColor: Colors.red);
                                    }
                                  });
                                }
                              },
                            ),
                        ]),
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

//check if the keyboard is open or not
  void _checkKeyboard(BuildContext context) {
    if (MediaQuery.of(context).viewInsets.bottom == 0.0) {
      context.read<AddTaskStateCubitCubit>().setKeyboardKey(key: false);

      debugPrint('keyboard is not open');
    } else {
      context.read<AddTaskStateCubitCubit>().setKeyboardKey(key: true);
      debugPrint('keyboard is open');
    }
  }
}
