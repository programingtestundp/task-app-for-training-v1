import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_task_management_app_1/models/task_model.dart';
import 'package:flutter_task_management_app_1/widgets/snke_bar_message_widget.dart';

import '../blocs/all tasks cubits/cubit/all_tasks_cubit.dart';
import '../utils/app_colors.dart';
import '../widgets/button_widget.dart';
import '../widgets/task_wedgit.dart';
import 'add_task_screen.dart';

class AllTasksScreen extends StatelessWidget {
  const AllTasksScreen({super.key});
  static const id = 'all_tasks_screen_id';

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);

    // Portrait or Landscape layout
    bool isPortrait = ScreenUtil().orientation == Orientation.portrait;

    return SafeArea(
      child: Scaffold(
        body: Column(children: [
          //header
          Container(
            height: isPortrait ? 190.h : 80.h,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/header1.jpg'),
                    fit: BoxFit.cover,
                    alignment: Alignment.bottomCenter)),
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(left: 10.w, top: 20.h),
            child: IconButton(
                onPressed: () => Navigator.of(context).pop(true),
                icon: const Icon(
                  Icons.arrow_back,
                  color: AppColors.secondaryColor,
                )),
          ),
          //tool bar
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.home,
                        color: AppColors.secondaryColor,
                      )),
                  SizedBox(
                    width: 8.w,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed(AddTask.id, arguments: {
                        'task': null,
                        'index': null,
                        'isRead': false,
                        'rout': RoutD.fromAllTasksScreen
                      });
                    },
                    child: CircleAvatar(
                      backgroundColor: Colors.black,
                      radius: 12.5.r,
                      child: const Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.calendar_month_sharp,
                        color: AppColors.secondaryColor,
                      )),
                  SizedBox(
                    width: 8.w,
                  ),
                  Text(
                    '2',
                    style: TextStyle(
                        fontSize: 20.sp, color: AppColors.secondaryColor),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 10.r,
          ),
          //tasks List
          Flexible(
            child: BlocBuilder<AllTasksCubit, AllTasksState>(
              builder: (context, state) {
                final tasksList = state.tasksList.reversed.toList();
                return ListView.builder(
                  itemCount: tasksList.length,
                  itemBuilder: (context, index) {
                    final task = tasksList[index];

                    return Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 12.w, vertical: 15.h),
                        child: Dismissible(
                          background: leftEditIcon(),
                          secondaryBackground: rightDeleteIcon(
                              isLoading: state.dismissStatusList[index] ==
                                  DismissStatus.deleting),
                          onUpdate: (details) {
                            context.read<AllTasksCubit>().onUpdateDismissible(
                                details: details, index: index);
                          },
                          onDismissed: (direction) {
                            debugPrint('onDismissed print');
                          },
                          confirmDismiss: (direction) async {
                            //confirm deleting
                            if (direction == DismissDirection.endToStart) {
                              final bool isDelete = await context
                                  .read<AllTasksCubit>()
                                  .confirmDelete(index: index)
                                  .then((value) {
                                if (!value) {
                                  context.showSnackBar(
                                      message: state.errorMessage,
                                      backgroundColor: Colors.red);
                                }
                                return value;
                              });

                              return isDelete;
                            }
                            // confirm edit
                            else if (direction == DismissDirection.startToEnd) {
                              modalBottomSheet(context,
                                  index: tasksList.length - index - 1,
                                  task: task);
                              return false;
                            }

                            return false;
                          },
                          key: ValueKey(task.id),
                          child: TaskWidget(
                            radius: state.radius,
                            text: task.task_name,
                            color: Colors.blueGrey,
                          ),
                        ));
                  },
                );
              },
            ),
          ),
        ]),
      ),
    );
  }

  Future<dynamic> modalBottomSheet(BuildContext context,
      {required TaskModel task, required int index}) {
    return showModalBottomSheet(
      backgroundColor: AppColors.mainColor.withOpacity(0.5),
      barrierColor: Colors.transparent,
      context: context,
      builder: (context) {
        return Container(
          width: double.maxFinite,
          height: 200.h,
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            ButtonWidget(
              text: 'View',
              color: AppColors.mainColor,
              textColor: Colors.white,
              onTap: () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pushNamed(AddTask.id, arguments: {
                  'task': task,
                  'index': index,
                  'isRead': true,
                  'rout': RoutD.fromAllTasksScreen
                });
              },
            ),
            SizedBox(height: 10.h),
            ButtonWidget(
              text: 'Edit',
              color: AppColors.mainColor,
              textColor: Colors.blueAccent,
              onTap: () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pushNamed(AddTask.id, arguments: {
                  'task': task,
                  'index': index,
                  'isRead': false,
                  'rout': RoutD.fromAllTasksScreen
                });
              },
            ),
          ]),
        );
      },
    );
  }

  // ........... Widgets ........... //
  Widget leftEditIcon() => Container(
        padding: EdgeInsets.only(left: 10.w),
        color: const Color(0xFF2e3253),
        alignment: Alignment.centerLeft,
        child: const Icon(
          Icons.edit,
          color: Colors.white,
        ),
      );

  Widget rightDeleteIcon({required bool isLoading}) => Container(
        padding: EdgeInsets.only(right: 10.w),
        color: Colors.red,
        alignment: isLoading ? Alignment.center : Alignment.centerRight,
        child: isLoading
            ? const CircularProgressIndicator(
                color: Colors.white,
              )
            : const Icon(
                Icons.delete,
                color: Colors.white,
              ),
      );
}
