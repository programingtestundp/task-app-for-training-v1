import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_task_management_app_1/screens/all_tasks_screen.dart';
import 'package:flutter_task_management_app_1/utils/app_colors.dart';

import '../blocs/all tasks cubits/cubit/all_tasks_cubit.dart';

import '../widgets/button_widget.dart';
import '../widgets/conform_dilog.dart';
import 'add_task_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  static const id = 'home_id';

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
    return WillPopScope(
      onWillPop: () async {
        final result = await confirmDialog(context,
                statText: "Do you want to ",
                specialText: "close",
                endText: ' ?',
                buttonColor: AppColors.mainColor,
                onTap: () => SystemNavigator.pop()) ??
            false;
        debugPrint('$result');
        return result;
      },
      child: Scaffold(
        body: BlocBuilder<AllTasksCubit, AllTasksState>(
          builder: (context, state) {
            if (state.appStatus == AppStatus.loading) {
              return const Center(child: CircularProgressIndicator());
            } else if (state.appStatus == AppStatus.loaded) {
              return homeBody(context);
            } else {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.w),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    const SizedBox(
                      width: double.maxFinite,
                    ),
                    Text(state.errorMessage),
                    SizedBox(
                      height: 10.h,
                    ),
                    ButtonWidget(
                        width: 100.w,
                        height: 40.h,
                        color: AppColors.mainColor,
                        text: 'Retry',
                        onTap: () {
                          context.read<AllTasksCubit>().retry();
                        },
                        textColor: Colors.white),
                    SizedBox(
                      height: 10.h,
                    ),
                    ButtonWidget(
                        width: 100.w,
                        height: 40.h,
                        color: Colors.red,
                        text: 'Close',
                        onTap: () {
                          exit(0);
                        },
                        textColor: Colors.white)
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Container homeBody(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/welcome.jpg'), fit: BoxFit.fill)),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0.w, vertical: 40.h),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                  text: 'Hello\n',
                  style: TextStyle(
                      color: AppColors.mainColor,
                      fontSize: 60.sp,
                      fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(
                      text: 'start your beautiful day',
                      style: TextStyle(
                        color: AppColors.mainColor,
                        fontSize: 16.sp,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 260.h),
              ButtonWidget(
                text: 'Add Task',
                color: AppColors.mainColor,
                textColor: Colors.white,
                onTap: () async {
                  Navigator.of(context).pushNamed(AddTask.id, arguments: {
                    'task': null,
                    'index': null,
                    'rout': null,
                    'isRead': false,
                  });
                },
              ),
              SizedBox(height: 10.h),
              ButtonWidget(
                  onTap: () {
                    Navigator.of(context).pushNamed(
                      AllTasksScreen.id,
                    );
                  },
                  text: 'View All',
                  color: Colors.white,
                  textColor: AppColors.smallTextColor),
            ]),
      ),
    );
  }
}
