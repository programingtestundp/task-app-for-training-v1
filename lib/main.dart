import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task_management_app_1/blocs/add%20task%20cubit/cubit/add_task_state_cubit_cubit.dart';

import 'blocs/all tasks cubits/cubit/all_tasks_cubit.dart';

import 'screens/home_screen.dart';
import 'utils/app_routs.dart';

void main() {
  runApp(MyApp(
    appRouter: AppRouter(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key, required this.appRouter});
  final AppRouter appRouter;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);

    return MultiBlocProvider(
      providers: [
        //control the state of add task screen elements
        BlocProvider(
          create: (context) => AddTaskStateCubitCubit(),
        ),
        //control the state of all tasks screen elements
        BlocProvider(
          create: (context) => AllTasksCubit()..initialState(),
        ),
      ],
      child: ScreenUtilInit(
        designSize: const Size(360, 640),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Tasks App by Jaafar Melhem',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            onGenerateRoute: appRouter.onGenerateRoute,
            home: child,
          );
        },
        child: const HomeScreen(),
      ),
    );
  }
}
